module.exports = app => {
    const question = require("../controllers/question.controller.js");
  
    var router = require("express").Router();

    // Retrieve all Logs
    router.get("/", question.findAll);
  
  
  
    app.use("/api/question", router);
  };