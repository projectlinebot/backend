module.exports = app => {
  const logs = require("../controllers/log.controller.js");

  var router = require("express").Router();

  // Create a new Log
  router.post("/", logs.create);

  // Retrieve all Logs
  router.get("/", logs.findAll);

  // Retrieve a single Log with id
  router.get("/:user", logs.findOne);

  // Update a Log with id
  router.put("/:id", logs.update);

  // Delete a Log with id
  router.delete("/:id", logs.delete);

  // Create a new Log
  router.delete("/", logs.deleteAll);

  app.use("/api/logs", router);
};