const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
var MongoClient = require('mongodb').MongoClient
var url = "mongodb+srv://admin:admin123456@cluster0.feort.mongodb.net/mydb";

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

var db = require("./app/models");
db.mongoose
  .connect(db.url, {
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome." });
});
app.post("/sendtext", (req, res) => {
  var datetime = require('node-datetime');
  var dt = datetime.create();
  var formattedDate = dt.format('d/m/y H:M');
  var id = req.body.id
  var text = req.body.text
  var displayname = req.body.displayName
  var datetime = formattedDate
  console.log(displayname)
  console.log(datetime)
  MongoClient.connect(url,function(err,db){
    if(err) throw err;
    var dbo =db.db("mydb")
    dbo.collection('logs').insertOne({user: displayname,text:text,datetime:datetime,type:'ADMIN'},function(err){
      if(err) throw err;
      console.log('1 document inserted (admin)')
      db.close();
    })
    updateDatabase()
  })

  function updateDatabase() {
    var mysort = {datetime: -1}
    MongoClient.connect(url, function (err, db) {
      if (err) throw err;
      var dbo = db.db("mydb");
        dbo.collection("logs").find().sort(mysort).toArray(function(err, result) {
          if(err) throw err;
          var data = result
          insertData(data)
          db.close();
        })
    });
  }
  function insertData(data) {
    var lastdata = data
    MongoClient.connect(url, function (err, db) {
      if (err) throw err;
      var dbo = db.db("mydb");
      dbo.collection("logs").drop(function(err, delok) {
        if(err) throw err;
        if(delok) console.log("Collection deleted")
        db.close();
      })
      dbo.createCollection("logs", function(err , res) {
        if(err) throw err;
        console.log("Collection created")
        db.close();
      })
      dbo.collection("logs").insertMany(lastdata, function(err, result) {
        if(err) throw err;
        console.log("insert complate")
        db.close();
      })
    })
  }
  // var text = req.params.text
  var request = require('request')
  var data = {
    to: id,
    messages: [
      {
        type: 'text',
        text: text
      }
    ]
  }
  console.log('Send message to line')
  request({
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer EyUa06YHmYxoz2kpQVJeNcm7GTj1QTAsImCsYaOKJRDK458meFtp5Zr/ZxKd2g2Pd2HM4qvHHV0+qd1On0K7qFrq9ZqsyAIWDT+YePuk9xVjWt/83BYHRMC3XrsA6ylxUcYWEApfqnePGX/m2NF/FAdB04t89/1O/w1cDnyilFU='
    },
    url: 'https://api.line.me/v2/bot/message/push',
    method: 'POST',
    body: data,
    json: true
  }, function (rErr, rRes, rBody) {
    if (rErr) {
      console.log('error')
      res.status(500).json({
        error: 'UNABLE_TO_SEND_LINE'
      })
    }
    if (rRes) {
      console.log('success')
      res.status(200).json({
        data: rBody
      })
    }
  })
});
require("./app/routes/turorial.routes")(app);
require("./app/routes/log.routes")(app);
require("./app/routes/customer.routes")(app);
require("./app/routes/question.routes")(app);
require("./app/routes/result.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
