const db = require("../models");
const Question = db.questions;

const getPagination = (page, size) => {
  const limit = size ? +size : 15;
  const offset = page ? page * limit : 0;

  return { limit, offset };
};


// Retrieve all Logs from the database.
exports.findAll = (req, res) => {
  const { page, size, idquestion } = req.query;
  var condition = idquestion
    ? { idquestion: { $regex: new RegExp(idquestion), $options: "i" } }
    : {};

  const { limit, offset } = getPagination(page, size);

  Question.paginate(condition, { offset, limit })
    .then((data) => {
      res.send({
        totalItems: data.totalDocs,
        questions: data.docs,
        totalPages: data.totalPages,
        currentPage: data.page - 1,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving customers.",
      });
    });
};
