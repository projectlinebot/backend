module.exports = (mongoose, mongoosePaginate) => {
    var schema = mongoose.Schema(
      {
        idquestion: String
      },
      { timestamps: true }
    );
  
    schema.method("toJSON", function() {
      const { __v, _id, ...object } = this.toObject();
      object.id = _id;
      return object;
    });
  
    schema.plugin(mongoosePaginate);
  
    const Question = mongoose.model("question", schema);
    return Question;
  };