module.exports = app => {
  const customers = require("../controllers/customer.controller.js");

  var router = require("express").Router();

  // Create a new Log
  router.post("/", customers.create);

  // Retrieve all Logs
  router.get("/", customers.findAll);

  // Retrieve a single Log with id
  router.get("/:user", customers.findOne);

  // Update a Log with id
  router.put("/:id", customers.update);

  // Delete a Log with id
  router.delete("/:id", customers.delete);

  // Create a new Log
  router.delete("/", customers.deleteAll);

  app.use("/api/customers", router);
};